#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
# ALWAYS provide not empty password, otherwise mysql-server installation hangs up
PASSWORD='admin'

echo "<?php phpinfo(); ?>" > /var/www/html/index.php

# install PPA 'ondrej/php'. The PPA is well known, and is relatively safe to use.
sudo apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:ondrej/php

# update / upgrade
sudo apt -y update
sudo apt -y upgrade

# install apache
sudo apt install -y apache2

# removing all old php dependencies (not needed on fresh setup).
#sudo apt-get remove -y php*

# installing php7.2 and php modules
sudo apt-get install -y php7.2 php7.2-cli php7.2-common libapache2-mod-php7.2 php7.2-fpm php7.2-curl php7.2-gd php7.2-bz2 php7.2-json php7.2-tidy php7.2-mbstring php-redis php-memcached

# install mysql and give password to installer
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt install -y mysql-server
sudo apt install -y php7.2-mysql

# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt install -y phpmyadmin

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/html"
    <Directory "/var/www/html">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

# enable mod_rewrite
sudo a2enmod rewrite

# restart apache
sudo service apache2 restart
# restart mysql
sudo service mysql restart

# install git
sudo apt install -y git

# install Composer
sudo apt install -y composer

# install NodeJS and NPM
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
