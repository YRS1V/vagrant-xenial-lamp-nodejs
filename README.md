# Simple LAMP Vagrant box

Simple LAMP environment inside Vagrant box. Based on official `ubuntu/xenial64` box.

## In the box:

- Ubuntu 16.04
- Apache 2.4
- MySQL 5.7
- PHP 7.2
- Node.js 10.x (with NPM)

### Additionally installed:

- PHP cURL
- phpMyAdmin
- Git
- Composer


## How to use:

After setup is finished, go to http://192.168.33.10/ in your browser. You should see the `phpinfo()` page.
`./www/`(or `/vagrant/www/` inside the box) is your "DocumentRoot".

`phpMyAdmin` is accessible at http://192.168.33.10/phpmyadmin/
